import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceuilComponent } from './presentation/pages/acceuil/acceuil.component';
import { ShopComponent } from './presentation/pages/shop/shop.component';

const routes: Routes = [
  { component: ShopComponent, path: 'shop/:idUser' },
  { component: AcceuilComponent, path: 'acceuil/:idUser' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
