import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public names : Array<string> =['Guillaume Lavergne','Narkmoud le dealer du coin','Jean Castex'];
  public idUser:number;
  constructor(private readonly route:ActivatedRoute) { 
  }

  ngOnInit(): void {
    this.idUser= Number(this.route.snapshot.paramMap.get('idUser'))-1;
  }

}
