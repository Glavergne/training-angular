import { Component, OnInit } from '@angular/core';
import { ShopService } from 'src/app/service/shop.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  public items: Array<any>;
  constructor(private readonly shopService: ShopService) { }

  async ngOnInit(): Promise<void> {
    this.items = await this.shopService.listItems();
    console.log(this.items);
  }

}
