import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShopService {
  constructor(private readonly httpClient: HttpClient) {}
  /**
   * listItems
   */
  public listItems(): Promise<any> {
    return this.httpClient.get('http://localhost:8080/items').toPromise();
  }
}
